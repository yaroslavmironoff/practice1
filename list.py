example_list_1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
example_list_2 = [1, 20, 4.3, 'word']
example_list_3 = list("qwertyuabcdefghijklmnopqrstuvwxyzupperintermediateword")
# list - изменяемый тип данных

print("Длина списка example_list_1 =", len(example_list_1))
print("Длина списка example_list_2 =", len(example_list_2))
print("Длина списка example_list_3 =", len(example_list_3))
print("Результат объединения первых двух списков:", example_list_1 + example_list_2)
print("Последний элемент каждого списка:", example_list_1[len(example_list_1) - 1], ",",
      example_list_2[len(example_list_2) - 1], ",", example_list_3[len(example_list_3) - 1])

example_list_2_str = str(example_list_2[0:len(example_list_2)])

print("Строка, которую можно получить из списка 2 с помощью команды join:", ' '.join(example_list_2_str))

example_list_1.insert(len(example_list_1), 8)
example_list_2.insert(len(example_list_2), 8)

print("Список 1 после добавления числа 8:", example_list_1)
print("Список 2 после добавления числа 8:", example_list_2)

example_list_3.pop(0)
example_list_3.pop(-1)

print("Список 3 после удаления первого и последнего элемента:", example_list_3)

example_list_3.sort()

print("Список 3 после сортировки:", example_list_3)
