import sys

#список
list1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

#множество
set1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

#кортеж
tuple1 = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

print("Размер list1 =", sys.getsizeof(list1))
print("Размер set1 =", sys.getsizeof(set1))
print("Размер tuple1 =", sys.getsizeof(tuple1))

print("Тип list1:", type(list1))
print("Тип set1:", type(set1))
print("Тип tuple1:", type(tuple1))
