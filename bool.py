tr = 1
fa = 0

# bool - неизменяемый тип данных

print("Переменная со значением true:", bool(tr))
print("Переменная со значением false:", bool(fa))
print("Результат сравнения двух переменных:", tr == fa)

empty_list = []

print("Результат сравнения пустого списка с каждой переменной:", empty_list == tr, empty_list == fa)
print("Результат сравнения строки 'khjh' с каждой переменной:", 'khjh' == tr, 'khjh' == fa)

empty_value = None

print("Результат сравнения empty_value с каждой переменной:", empty_value == tr, empty_value == fa)
