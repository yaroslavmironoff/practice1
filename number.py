import math

#int - неизменяемый тип данных
number = 17

print("Число 17 в двоичном коде =", bin(number))
print("Число 17 в 16-ой системе =", hex(number))
print("Корень из числа 17 =", math.sqrt(number))
print("Остаток от деления числа 17 на 5 =", number % 5)
print("Результат деления числа 17 на 3 и округленный до целых =", round(number / 3))

