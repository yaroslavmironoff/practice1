london = {'name': 'London1', 'location': 'London Str', 'vendor': 'Cisco'}

#dictionary - изменяемый тип данных

london_co = {
    'r1': {
        'hostname': 'london_r1',
        'location': '21 New Globe Walk',
        'vendor': 'Cisco',
        'model': '4451',
        'ios': '15.4',
        'ip': '10.255.0.1'
    },
    'r2': {
        'hostname': 'london_r2',
        'location': '21 New Globe Walk',
        'vendor': 'Cisco',
        'model': '4451',
        'ios': '15.4',
        'ip': '10.255.0.2'
    },
    'sw1': {
        'hostname': 'london_sw1',
        'location': '21 New Globe Walk',
        'vendor': 'Cisco',
        'model': '3850',
        'ios': '3.6.XE',
        'ip': '10.255.0.101'
    }
}

print("Количество элементов в словаре london =", len(london))
print("Количество словарей в словаре london_co =",len(london_co))
print("Количество элементов в словаре london_co =",len(london_co['r1'])+len(london_co['r2'])+len(london_co['sw1']))
print("Ключи словаря london:", london.keys())
print("Ключи словаря london_co:", london_co.keys())
print("Ключи словаря london_co['r1']:", london_co['r1'].keys())
print("Ключи словаря london_co['r2']:", london_co['r2'].keys())
print("Ключи словаря london_co['sw1']:", london_co['sw1'].keys())
print("Значение ключа vendor словаря london:", london['vendor'])

#добавление в словарь london пары ‘meta’: ‘object’
london['meta'] = 'object'

print("Все значения словаря london:", london.values())
print("Все пары словаря london:", london.items())

#удаление из словаря london_co ключа sw1
del london_co['sw1']
#print(london_co.items())

print("Ключи словаря london_co до изменениий:", london_co.keys())
#print(london_co.pop('r2'))
london_co['1234'] = london_co.pop('r2')
print("Ключи словаря london_co после обновления ключа r2:", london_co.keys())
