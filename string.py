string1 = "mkmkmklmlma"
#str - неизменяемый тип данных

print("Первый и последний элемент строки:", string1[0], ";", string1[len(string1)-1])
print("Длина строки: ", len(string1))
print("Элементы строки с 2 по 5: ", string1[1:5])
print("Все символы в верхнем регистре:", string1.upper())
print("Количество букв m =", string1.count('m'))
print("Замена в строке ‘ma’ на ‘am’:", string1.replace('ma','am'))
print("Результат после пореза строки по символу 'm':", string1.split('m'))